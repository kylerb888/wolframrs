use crate::communication::PodResponse;
use net::TcpStream;
use tungstenite::{WebSocket, client, error::ProtocolError, stream::Stream};
mod error;
use error::Error;
use std::{*, io::Write};
mod communication;
use tungstenite::Error::Protocol;
use native_tls::TlsStream;
fn process_query(arg: &str, num: i32, stream: &mut WebSocket<Stream<TcpStream, TlsStream<TcpStream>>>) -> Result<(), Error>
{
    // Refresh stream when wolfram closes the socket after inactivity
    let query = communication::send_init(&arg)?;
    loop {
        match stream.write_message(query.to_owned()) {
            Ok(_) => {},
            Err(Protocol(ProtocolError::ResetWithoutClosingHandshake)) => {
                *stream = client::connect("wss://www.wolframalpha.com/n/v1/api/fetcher/results")?.0;
                continue
            }
            other => return other.map_err(Error::Tungstenite).map(|_| ()),
        }
        break
    }

    print!("--------------------------------------------------\n\n- {}.\n", num);
    loop {
        let read = match stream.read_message() {
            Ok(val) => val,
            Err(Protocol(ProtocolError::ResetWithoutClosingHandshake)) => {
                *stream = client::connect("wss://www.wolframalpha.com/n/v1/api/fetcher/results")?.0;
                continue
            }
            other => return other.map_err(Error::Tungstenite).map(|_| ()),
        };
        let received = read.to_text()?;
        let val: PodResponse = serde_json::from_str(received)?;
        if val._type == "queryComplete" {
            break;
        }
        if let Some(mut pods) = val.pods {
            pods.retain(|pod| !matches!(&*pod.title, "Plot" | "Number line" | "Inequality plot"));
            for pod in pods {
                print!("{}\n", pod);
            }
        }
    }
    Ok(())
}
fn main() -> Result<(), Error> {
    let args: Vec<String> = env::args().skip(1).collect();

    let mut stream = client::connect("wss://www.wolframalpha.com/n/v1/api/fetcher/results")?.0;
    if args.is_empty() {
        let stdin = io::stdin();
        let mut stdout = io::stdout();
        let mut input = Vec::new();
        let mut get = String::new();
        loop {

            loop {
                get.clear();
                print!("> ");
                stdout.flush()?;
                
                stdin.read_line(&mut get)?;
                let test = get.trim();
                
                
                match test {
                    "quit" | "q" | "exit" => return Ok(()),
                    "send" | "done" => break,
                    _ => input.push(test.to_owned()),
                }
            }
            for (arg, num) in input.iter().zip(1..) {
                process_query(arg, num, &mut stream)?;
            }
            input.clear();        
        }

        

    }
    for (arg, num) in args.into_iter().zip(1..) {
        process_query(&arg, num, &mut stream)?;
    }
    
    print!("--------------------------------------------------\n");

    Ok(())
}
