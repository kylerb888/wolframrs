use std::fmt::Display;

use tungstenite::Message;

use crate::error::Error;
use serde::{Deserialize, Serialize};

impl Default for WolframInitMessage {
    fn default() -> Self {
        Self {
            _type: String::from("newQuery"),
            locationId: String::from("ddnsf1"),
            language: String::from("en"),
            displayDebuggingInfo: false,
            yellowIsError: false,
            input: String::default(),
            i2d: false,
            assumption: Vec::new(),
            file: None,
        }
    }
}

impl Default for Init {
    fn default() -> Self {
        Self {
            _type: String::from("init"),
            lang: String::from("en"),
            exp: u32::MAX,
            displayDebuggingInfo: false,
            messages: vec![WolframInitMessage::default()],
            input: String::default(),
            i2d: false,
            assumption: Vec::default(),
            file: Option::default(),
        }
    }
}

#[derive(Deserialize, Debug)]
pub(crate) struct PodResponse {
    #[serde(rename = "type")]
    pub(crate) _type: String,
    pub(crate) pods: Option<Vec<Pod>>,
}

impl Display for Pod {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}:\n", self.title)?;
        for subpod in &self.subpods {
            write!(f, "  {}: {}\n", subpod.title, subpod.plaintext)?
        }
        Ok(())
    }
}
#[derive(Deserialize, Debug)]
pub(crate) struct Pod {
    pub(crate) title: String,
    #[serde(default)]
    pub(crate) subpods: Vec<SubPod>,
}
#[derive(Deserialize, Debug)]
pub(crate) struct SubPod {
    pub(crate) title: String,
    pub(crate) plaintext: String,
}
#[derive(Serialize, Debug)]
struct WolframInitMessage {
    #[serde(rename = "type")]
    _type: String,
    locationId: String,
    language: String,
    displayDebuggingInfo: bool,
    yellowIsError: bool,
    input: String,
    i2d: bool,
    assumption: Vec<String>,
    file: Option<Vec<u8>>,
}
#[derive(Serialize, Debug)]
struct Init {
    #[serde(rename = "type")]
    _type: String,
    lang: String,
    exp: u32,
    displayDebuggingInfo: bool,
    messages: Vec<WolframInitMessage>,
    input: String,
    i2d: bool,
    assumption: Vec<String>,
    file: Option<Vec<u8>>,
}

pub(crate) fn send_init(query: &str) -> Result<Message, Error> {
    let mut init = Init {
        input: query.to_owned(),
        ..Default::default()
    };
    for message in &mut init.messages {
        message.input = query.to_owned();
    }
    let as_string = serde_json::to_string(&init)?;
    Ok(Message::Text(as_string))
}
